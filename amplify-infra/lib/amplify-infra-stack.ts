import { Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";

import * as cdk from "aws-cdk-lib";
import * as amplify from "aws-cdk-lib/aws-amplify";
import * as S3 from "aws-cdk-lib/aws-s3";
import * as iam from "aws-cdk-lib/aws-iam";

// test fargate container deploy
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as ecs from "aws-cdk-lib/aws-ecs";
import * as ecs_patterns from "aws-cdk-lib/aws-ecs-patterns";

// test lambda deploy
import * as lambda from "aws-cdk-lib/aws-lambda";

import * as apigateway from "aws-cdk-lib/aws-apigatewayv2";

import { RegionDict, SageMakerEndpointConstruct } from "./sagemaker_endpoint";

// get interface (prob better in external file)
import { HuggingfaceSagemakerConfig } from "./sagemaker_endpoint";

import * as config from "./huggingface_sagemaker_config.json";

// test aws codebuild aws pipeline
import * as codebuild from "aws-cdk-lib/aws-codebuild";
import * as codepipeline from "aws-cdk-lib/aws-codepipeline";
import * as codepipeline_actions from "aws-cdk-lib/aws-codepipeline-actions";

export class AmplifyInfraStack extends Stack {
  // private readonly huggingface_sagemaker_config: HuggingfaceSagemakerConfig;

  // // policies based on https://docs.aws.amazon.com/sagemaker/latest/dg/sagemaker-roles.html#sagemaker-roles-createmodel-perms
  // private readonly iam_sagemaker_actions = [
  //     "sagemaker:*",
  //     "ecr:GetDownloadUrlForLayer",
  //     "ecr:BatchGetImage",
  //     "ecr:BatchCheckLayerAvailability",
  //     "ecr:GetAuthorizationToken",
  //     "cloudwatch:PutMetricData",
  //     "cloudwatch:GetMetricData",
  //     "cloudwatch:GetMetricStatistics",
  //     "cloudwatch:ListMetrics",
  //     "logs:CreateLogGroup",
  //     "logs:CreateLogStream",
  //     "logs:DescribeLogStreams",
  //     "logs:PutLogEvents",
  //     "logs:GetLogEvents",
  //     "s3:CreateBucket",
  //     "s3:ListBucket",
  //     "s3:GetBucketLocation",
  //     "s3:GetObject",
  //     "s3:PutObject",
  // ];

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // // parse config
    // this.huggingface_sagemaker_config = config; // prop typecasing would prob be better

    // // Stack Variables

    // // creates new iam role for sagemaker using `iam_sagemaker_actions` as permissions
    // let execution_role = new iam.Role(this, "hf_sagemaker_execution_role", {
    //       assumedBy: new iam.ServicePrincipal("sagemaker.amazonaws.com")
    //     }
    // );
    // execution_role.addToPolicy(new iam.PolicyStatement({
    //   resources: ['*'],
    //   actions: this.iam_sagemaker_actions,
    // }))

    // let execution_role_arn = execution_role.roleArn;

    // // SageMaker Endpoint
    // const sageMakerEndpoint = new SageMakerEndpointConstruct(this, "SageMakerEndpoint", {
    //   region: this.huggingface_sagemaker_config.region,
    //   region_nr: this.huggingface_sagemaker_config.region_dict[this.huggingface_sagemaker_config.region as keyof RegionDict],
    //   huggingface_model: this.huggingface_sagemaker_config.huggingface_model_name,
    //   model_data: this.huggingface_sagemaker_config.model_data,
    //   huggingface_task: this.huggingface_sagemaker_config.task,
    //   execution_role_arn: execution_role_arn,
    //   latest_transformer_version: this.huggingface_sagemaker_config.LATEST_PYTORCH_VERSION,
    //   latest_pytorch_version: this.huggingface_sagemaker_config.LATEST_PYTORCH_VERSION
    // });

    ///////////////////////////////

    // Part 2 - Creation of the Amplify Application
    // const amplifyApp = new amplify.App(this, "sample-react-app ", {
    //   sourceCodeProvider: new amplify. GitHubSourceCodeProvider({
    //     owner: "[Repository-Owner]",
    //     repository: "[Repository-Name]",
    //     oauthToken: cdk.SecretValue.secretsManager("[Secret-Name]", {
    //       jsonField: "[Secret-Key]",
    //     }),
    //   }),
    // });
    // const masterBranch = amplifyApp.addBranch("master");

    // const amplifyApp = new amplify.CfnApp(this, "test-app", {
    //   name: "your-amplify-console-app-name",
    //   repository: "https://github.com/raphywink/amplify-sample-app.git",
    //   oauthToken: "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",
    //   basicAuthConfig: {
    //     enableBasicAuth: true,
    //     password: "pewpewkabang",
    //     username: "mrawesomepants",
    //   },
    // });

    // new amplify.CfnBranch(this, "MainBranch", {
    //   appId: amplifyApp.attrAppId,
    //   branchName: "main", // you can put any branch here (careful, it will listen to changes on this branch)
    // });

    // fargate RASA NLU container deployments

    // const vpc = new ec2.Vpc(this, "MyVpc", {
    //   maxAzs: 3 // Default is all AZs in region
    // });

    // const cluster = new ecs.Cluster(this, "MyCluster", {
    //   vpc: vpc
    // });

    // // Create a load-balanced Fargate service and make it public
    // new ecs_patterns.ApplicationLoadBalancedFargateService(this, "MyFargateService", {
    //   cluster: cluster, // Required
    //   cpu: 512, // Default is 256
    //   desiredCount: 1, // Default is 1
    //   taskImageOptions: { image: ecs.ContainerImage.fromRegistry("amazon/amazon-ecs-sample") },
    //   memoryLimitMiB: 2048, // Default is 512
    //   publicLoadBalancer: true // Default is false

    // });

    // const role = new iam.Role(this, 'Role', {
    //   assumedBy:
    // });

    // const myRole = new iam.Role(this, 'LambdaNodeStackRole', {
    //   assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
    // });

    /////////////////////////////////////
    // create deepgram lambda function (should acts as relay between deepgram & amplify app)
    // const deepgramLambda = new lambda.Function(this, 'iuv-api-handler-stack', {
    //   code: lambda.Code.fromAsset('./src/deepgramLambda'),
    //   functionName: 'iuv-api-handler',
    //   handler: 'index.handler',
    //   memorySize: 1024,
    //   runtime: lambda.Runtime.NODEJS_14_X,
    //   timeout: cdk.Duration.seconds(300),
    //   environment: {
    //     'CodeVersionString': 'v100',
    //   }
    // });

    // deepgramLambda.role!.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("service-role/AWSLambdaBasicExecutionRole"));
    // deepgramLambda.role!.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("SecretsManagerReadWrite"));
    // deepgramLambda.role!.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonAPIGatewayInvokeFullAccess"));

    //////////////////////////////////
    // API Gateway websocket stuff
    // see: https://www.youtube.com/watch?v=BcWD-M2PJ-8
    // and: https://stackoverflow.com/questions/63416026/aws-cdk-construct-in-c-sharp-for-api-gateway-websockets

    // SIC!!!! I can't get this to work ... tried every setting and no dice
    // also don't understand how the integrationUri is constructed

    // let integrationType = "AWS_PROXY";

    // // create API
    // const api = new apigateway.CfnApi(this, 'MyCfnApi', {
    //   name: "deepgramWSrelay",
    //   protocolType: "WEBSOCKET",
    //   routeSelectionExpression: "request.body.action",
    // });

    // ///////////////////////
    // // create integrations

    // // $connect lambda integration
    // const connectIntegration = new apigateway.CfnIntegration(this, "connect-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // // $disconnect lambda integration
    // const disconnectIntegration = new apigateway.CfnIntegration(this, "disconnect-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // // $default lambda integration
    // const defaultIntegration = new apigateway.CfnIntegration(this, "default-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // // $setName lambda integration
    // const setNameIntegration = new apigateway.CfnIntegration(this, "setName-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // // $sendPublic lambda integration
    // const sendPublicIntegration = new apigateway.CfnIntegration(this, "sendPublic-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // // $sendPrivate lambda integration
    // const sendPrivateIntegration = new apigateway.CfnIntegration(this, "sendPrivate-lambda-integration", {
    //   apiId: api.ref,
    //   integrationType: integrationType,
    //   integrationUri: "arn:aws:apigateway:" + config["region"] + ":lambda:path/2015-03-31/functions/" + deepgramLambda.functionArn + "/invocations",
    //   // credentialsArn: deepgramLambda.role!.roleArn,
    // });

    // /////////////////
    // // create routes

    // // $connect route definition
    // const connectRoute = new apigateway.CfnRoute(this, "connect-route", {
    //   apiId: api.ref,
    //   routeKey: "$connect",
    //   authorizationType: "NONE",
    //   target: "integrations/" + connectIntegration.ref,
    // });

    // // $disconnect route definition
    // const disconnectRoute = new apigateway.CfnRoute(this, "disconnect-route", {
    //   apiId: api.ref,
    //   routeKey: "$disconnect",
    //   authorizationType: "NONE",
    //   target: "integrations/" + disconnectIntegration.ref,
    // });

    // const defaultRoute = new apigateway.CfnRoute(this, "default-route", {
    //   apiId: api.ref,
    //   routeKey: "$default",
    //   authorizationType: "NONE",
    //   target: "integrations/" + defaultIntegration.ref,
    // });

    // const setNameRoute = new apigateway.CfnRoute(this, "setName-route", {
    //   apiId: api.ref,
    //   routeKey: "setName",
    //   authorizationType: "NONE",
    //   target: "integrations/" + setNameIntegration.ref,
    // });

    // const sendPublicRoute = new apigateway.CfnRoute(this, "sendPublic-route", {
    //   apiId: api.ref,
    //   routeKey: "sendPublic",
    //   authorizationType: "NONE",
    //   target: "integrations/" + sendPublicIntegration.ref,
    // });

    // const sendPrivateRoute = new apigateway.CfnRoute(this, "sendPrivate-route", {
    //   apiId: api.ref,
    //   routeKey: "sendPrivate",
    //   authorizationType: "NONE",
    //   target: "integrations/" + sendPrivateIntegration.ref,
    // });

    // ///////////////////////////////////////////
    // // Finishing touches on the API definition
    // const deployment = new apigateway.CfnDeployment(this, `myCfnApi-deployment`, {
    //   apiId: api.ref,
    // });

    // new apigateway.CfnStage(this, `myCfnApi-stage`, {
    //   apiId: api.ref,
    //   autoDeploy: true,
    //   deploymentId: deployment.ref,
    //   stageName: "production"
    // });

    ///////////////////////////////
    // IUV-Frontend: bitbucket -> codepipeline -> S3 bucket CI/CD workflow

    // AWS CodeBuild artifacts
    const outputSources = new codepipeline.Artifact();
    const outputWebsite = new codepipeline.Artifact();

    // AWS CodePipeline pipeline
    const pipeline = new codepipeline.Pipeline(this, "Pipeline", {
      pipelineName: "MyWebsite",
      restartExecutionOnUpdate: true,
    });

    // AWS CodePipeline stage to clone sources from bitbucket repository
    pipeline.addStage({
      stageName: "Source",
      actions: [
        new codepipeline_actions.CodeStarConnectionsSourceAction({
          actionName: "Checkout",
          owner: "raphywink",
          repo: "amplify-sample-app",
          output: outputSources,
          branch: "main", // the branch you deploy from
          connectionArn:
            "arn:aws:codestar-connections:us-east-1:212206538616:connection/0df38b4e-3a76-404d-9732-2d98d43bb672", // hast to created in console
        }),
      ],
    });
    // AWS CodePipeline stage to build website and CDK resources
    pipeline.addStage({
      stageName: "Build",
      actions: [
        // AWS CodePipeline action to run CodeBuild project
        new codepipeline_actions.CodeBuildAction({
          actionName: "Website",
          project: new codebuild.PipelineProject(this, "BuildWebsite", {
            projectName: "MyWebsite",
            buildSpec: codebuild.BuildSpec.fromSourceFilename("./amplify.yml"),
          }),
          input: outputSources,
          outputs: [outputWebsite],
        }),
      ],
    });
    // Amazon S3 bucket to store website
    const bucketWebsite = new S3.Bucket(this, "IUV-Frontend-Files", {
      websiteIndexDocument: "index.html",
      websiteErrorDocument: "error.html",
      publicReadAccess: true,
    });

    // AWS CodePipeline stage to deploy website and CDK resources
    pipeline.addStage({
      stageName: "Deploy",
      actions: [
        // AWS CodePipeline action to deploy website to S3
        new codepipeline_actions.S3DeployAction({
          actionName: "IUV-Frontend",
          input: outputWebsite,
          bucket: bucketWebsite,
        }),
      ],
    });
  }
}
