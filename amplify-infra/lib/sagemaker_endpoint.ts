import * as cdk from 'aws-cdk-lib';
import * as sagemaker from 'aws-cdk-lib/aws-sagemaker';
import { Construct } from 'constructs';

export interface SageMakerEndpointProps {
    /** the function for which we want to count url hits **/
    region: string,
    region_nr: string,
    huggingface_model: string,
    model_data: string,
    huggingface_task: string,
    execution_role_arn: string,
    latest_transformer_version: string
    latest_pytorch_version: string
};

export interface RegionDict {
    "af-south-1": string,
    "ap-east-1": string,
    "ap-northeast-1": string,
    "ap-northeast-2": string,
    "ap-northeast-3": string,
    "ap-south-1": string,
    "ap-southeast-1": string,
    "ap-southeast-2": string,
    "ca-central-1": string,
    "cn-north-1": string,
    "cn-northwest-1": string,
    "eu-central-1": string,
    "eu-north-1": string,
    "eu-south-1": string,
    "eu-west-1": string,
    "eu-west-2": string,
    "eu-west-3": string,
    "me-south-1": string,
    "sa-east-1": string,
    "us-east-1": string,
    "us-east-2": string,
    "us-gov-west-1": string,
    "us-iso-east-1": string,
    "us-west-1": string,
    "us-west-2": string
};

export interface HuggingfaceSagemakerConfig {
    huggingface_model_name: string,
    model_data: string,
    task: string,
    LATEST_PYTORCH_VERSION: string,
    LATEST_TRANSFORMERS_VERSION: string,
    region: string,
    region_dict: RegionDict
};


export class SageMakerEndpointConstruct extends Construct {

    private readonly props: SageMakerEndpointProps;
    private model_name: string;
    private endpoint_config_name: string;
    private endpoint_name: string;

    private image_uri: string;

    constructor(scope: Construct, id: string, props: SageMakerEndpointProps) {
        super(scope, id);

        this.props = props;

        // TODO read these from props instead
        this.model_name = "model-" + this.props.huggingface_model;
        this.endpoint_config_name = "config-" + this.props.huggingface_model;
        this.endpoint_config_name = "endpoint-" + this.props.huggingface_model;
     
        // creates the image_uir based on the instance type and region
        this.image_uri = this.get_image_uri(
            this.props.region,
            this.props.region_nr,
            this.props.latest_transformer_version,
            this.props.latest_pytorch_version,
            false)

        // defines and creates container configuration for deployment
        let container_environment = {
            "HF_TASK": props.huggingface_task,
            "HF_MODEL_ID": props.huggingface_model
        };

        const container: sagemaker.CfnModel.ContainerDefinitionProperty = {
            environment: container_environment,
            image: this.image_uri,
            modelDataUrl: this.props.model_data
        };

        // creates SageMaker Model Instance
        const model = new sagemaker.CfnModel(this, 'MyCfnModel', {
            executionRoleArn: this.props.execution_role_arn,
            primaryContainer: container,
            modelName: this.model_name
        });

        // Creates SageMaker Endpoint configurations
        // endpoint_configuration = sagemaker.CfnEndpointConfig(
        //     self,
        //     "hf_endpoint_config",
        //     endpoint_config_name=endpoint_config_name,
        //     production_variants=[
        //         sagemaker.CfnEndpointConfig.ProductionVariantProperty(
        //             initial_variant_weight=1.0,
        //             variant_name=model.model_name,
        //             model_name=model.model_name,
        //             serverless_config=sagemaker.CfnEndpointConfig.ServerlessConfigProperty(
        //                 max_concurrency=8, memory_size_in_mb=6144
        //             ),
        //         )
        //     ],
        // )

        let serverless_config: sagemaker.CfnEndpointConfig.ServerlessConfigProperty = {
            maxConcurrency: 8,
            memorySizeInMb: 6144
        }

        // let production_variant: sagemaker.CfnEndpointConfig.ProductionVariantProperty = {
        //     initialVariantWeight: 1.0,
        //     variantName: model.modelName,
        //     modelName: model.modelName,
        //     serverlessConfig: serverless_config
        // }

        // const endpoint_configuration = new sagemaker.CfnEndpointConfig(this, "hf_endpoint_config", {
        //     endpointConfigName: this.endpoint_config_name,
        //     productionVariants: [production_variant]
        // });


        // # Creates Real-Time Endpoint
        // endpoint = sagemaker.CfnEndpoint(
        //     self,
        //     "hf_endpoint",
        //     endpoint_name=endpoint_name,
        //     endpoint_config_name=endpoint_configuration.endpoint_config_name,
        // )

        // # adds depends on for different resources
        // endpoint_configuration.node.add_dependency(model)
        // endpoint.node.add_dependency(endpoint_configuration)

        // # construct export values
        // self.endpoint_name = endpoint.endpoint_name
        // self.model_name = parsed_model_name

    }

    private get_image_uri = (
        region: string,
        region_nr: string,
        transformers_version: string,
        pytorch_version: string,
        use_gpu: boolean) => {
            let repository: string;
            let tag: string;

            repository = region_nr + ".dkr.ecr." + region + ".amazonaws.com/huggingface-pytorch-inference";
            if(use_gpu){
                tag = pytorch_version + "-transformers" + transformers_version + "-gpu-py38-cu111-ubuntu20.04"
            } else {
                tag = pytorch_version + "-transformers" + transformers_version + "-cpu-py38-ubuntu20.04"
            }
            
            return repository + ":" + tag;

    }
}

function bla() {
    console.log('sup');
}
// short test -> `node filename.js` (to run js file compiled with `npm run watch`)
if (typeof require !== 'undefined' && require.main === module) {
    bla();
    
}
