## Deepgram lambda

lambda function to relay websocket stream to deepgram api to perform API

### prerequ.

install lambda-local for local testing

```
npm install -g lambda-local
```

### run locally

```
lambda-local -l index.js -h handler --watch 8008
```

### test POST request (won't work in final version)

```
curl --request POST \
  --url http://localhost:8008/ \
  --header 'content-type: application/json' \
  --data '{
        "event": {
                "key1": "value1",
                "key2": "value2",
                "key3": "value3"
        }
}'
```

