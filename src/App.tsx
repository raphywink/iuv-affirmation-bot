import { useState, useRef, useEffect, FC } from "react";
import { observer } from "mobx-react";
import MD5 from "crypto-js/md5";
import { Transition } from "@headlessui/react";

import "./App.css";
import IuvFace from "./components/IuvFace/IuvFace";

import { inputStreamProcessor } from "./utils/audio/InputStreamProcessor";
import VUIlogo from "./components/VUIlogo/VUIlogo";
import RecordButton from "./components/RecordButton/RecordButton";
import WelcomeModal from "./components/WelcomeModal/WelcomeModal";

import AudioElement from "./components/AudioElement/AudioElement";

import ChatWidget from "./components/ChatWidget/ChatWidget";
import PromptModal from "./components/PromptModal/PromptModal";
import { client } from "./components/ChatWidget/client";

import useKeyPress from "./utils/useKeyPress";

const App: FC = observer(() => {
  const [recording, setRecording] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [isp, setIsp] = useState(inputStreamProcessor);
  const [currentPlaybackSrcUrl, setCurrentPlaybackSrcUrl] = useState("");
  const [asrCurResult, setAsrCurResult] = useState("test 123");
  const [botResponse, setBotResponse] = useState("");

  // check if conversation has changed
  useEffect(() => {
    // console.log("client.conversation changed");
    if (!client.conversation[client.conversation.length - 1]) return;
    let convPart = client.conversation[client.conversation.length - 1];
    if (convPart.type == "request") return;
    setCurrentPlaybackSrcUrl(
      "/audio/" + MD5(convPart.data.text).toString() + "_trim.mp3"
    );
  }, [client.conversation[client.conversation.length - 1]]);

  const setRecordingWrapper = async (state: boolean) => {
    setRecording(state);
    if (state) {
      isp.setRecording(true);
    } else {
      isp.setRecording(false);
    }
  };

  const init = () => {
    // this should be set by bot but I can't get it to reset using the converse API
    // setCurrentPlaybackSrcUrl("audio/9348ae7851cf3ba798d9564ef308ec25_trim.mp3");
    setCurrentPlaybackSrcUrl("audio/welcome_message_trim.mp3");
  };

  const onKeyPress = (event: KeyboardEvent) => {
    setCurrentPlaybackSrcUrl("");
  };

  useKeyPress(["Escape", " "], onKeyPress);

  return (
    <div className="App bg-gray-800">
      <WelcomeModal init={init}></WelcomeModal>
      {/* main grid layout */}
      <div className="grid place-items-center h-screen grid-rows-4">
        {/* first row in grid */}
        <VUIlogo></VUIlogo>
        {/* second row in grid */}
        {currentPlaybackSrcUrl !== "" ? (
          <AudioElement
            src={currentPlaybackSrcUrl}
            setSrc={setCurrentPlaybackSrcUrl}
          ></AudioElement>
        ) : (
          <IuvFace></IuvFace>
        )}
        {/* third row in grid */}
        <div className="text-cyan-300 animate-bounce">
          {client.conversation.length > 0 &&
          client.conversation[client.conversation.length - 1].type ===
            "response" ? (
            <>
              {currentPlaybackSrcUrl !== ""
                ? client.conversation[client.conversation.length - 1].data.text
                : ""}
            </>
          ) : (
            <>{client.conversation.length > 0 ? "..." : ""}</>
          )}
        </div>
        {/* fourth row in grid */}
        <div className="flex flex-col flex-shrink-0 mt-auto justify-center items-center mb-16 ">
          <div className="mb-4 px-8">
            <p className="text-base text-center text-gray-800 dark:text-gray-400">
              {inputStreamProcessor.asrCurResult}
            </p>
          </div>
          <RecordButton
            setRecordingWrapper={setRecordingWrapper}
            disabled={currentPlaybackSrcUrl !== "" || client.waitingForResponse}
          ></RecordButton>
          <Transition
            show={currentPlaybackSrcUrl !== ""}
            enter="transition-opacity duration-75"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity duration-150"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="text-cyan-300 text-xs">
              Press ESC or SPACEBAR
              <br />
              to stop playback
            </div>
          </Transition>
        </div>
        <ChatWidget
          disabled={
            currentPlaybackSrcUrl !== "" ||
            recording ||
            client.waitingForResponse
          }
        ></ChatWidget>
        <PromptModal></PromptModal>
      </div>
    </div>
  );
});

export default App;
