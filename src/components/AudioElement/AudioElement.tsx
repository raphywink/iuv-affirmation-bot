import { FC, Fragment, useRef, useState } from "react";
import { PlayBackHandler } from "../../utils/audio/PlaybackHandler";

const AudioElement: FC<{ src: string; setSrc: Function }> = (props: {
  src: string;
  setSrc: Function;
}) => {
  const [pbh, setPbh] = useState(new PlayBackHandler());
  const [audioCtx, setAudioCtx] = useState(new AudioContext());
  const [playing, setPlaying] = useState(false);

  const audioElementRef = useRef<HTMLMediaElement>(null);
  const canvasElementRef = useRef<HTMLCanvasElement>(null);

  const WIDTH = 200;
  const HEIGHT = 200;

  const PI = 3.141592653589793;

  const onEnded = () => {
    console.log("onEnded");
    setPlaying(false);
    audioCtx.close();
    props.setSrc("");
  };

  const onPlay = () => {
    console.log("onPlay");
    setPlaying(true);
    ////////////////////////////////////
    // create audio processing tree node
    // var audioCtx = new AudioContext();
    var sourceNode = audioCtx.createMediaElementSource(
      audioElementRef.current!
    );
    var gainNode = audioCtx.createGain();
    // Create analyser
    var analyser = audioCtx.createAnalyser();
    analyser.smoothingTimeConstant = 0.99;
    analyser.fftSize = 512;

    // Create ScriptProcessorNode
    var scriptProcessorNode = audioCtx.createScriptProcessor(512, 1, 1);

    // connect everything up
    sourceNode.connect(gainNode);
    gainNode.connect(analyser);
    analyser.connect(audioCtx.destination);

    // set gain while playing
    // setTimeout(() => {
    //   gainNode.gain.setValueAtTime(0.1, audioCtx.currentTime);
    // }, 3000);

    var canvasCtx = canvasElementRef.current!.getContext("2d");
    canvasCtx!.clearRect(
      0,
      0,
      canvasElementRef.current!.width,
      canvasElementRef.current!.height
    );

    analyser.fftSize = 2048;
    var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);
    analyser.getByteTimeDomainData(dataArray);

    const wFunction = (length: number, index: number) => {
      return Math.cos((PI * index) / (length - 1) - PI / 2);
    };

    const draw = () => {
      if (!canvasElementRef.current) return;
      let drawVisual = requestAnimationFrame(draw);
      analyser.getByteTimeDomainData(dataArray);

      canvasCtx!.fillStyle = "rgb(31 41 55)";
      canvasCtx!.fillRect(0, 0, WIDTH, HEIGHT);

      canvasCtx!.lineWidth = 2;
      canvasCtx!.strokeStyle = "rgb(165 243 252)";
      // canvasCtx!.filter = "blur(2px)";

      canvasCtx!.beginPath();

      var sliceWidth = (WIDTH * 1.0) / bufferLength;
      var x = 0;

      for (var i = 0; i < bufferLength; i++) {
        var v = dataArray[i] / 128.0;
        // v *= wFunction(bufferLength, i);
        var y = (v * HEIGHT) / 2;

        if (i === 0) {
          canvasCtx!.moveTo(x, y);
        } else {
          canvasCtx!.lineTo(x, y);
        }

        x += 1; //sliceWidth;
      }

      canvasCtx!.lineTo(
        canvasElementRef.current!.width,
        canvasElementRef.current!.height / 2
      );
      canvasCtx!.stroke();
    };
    draw();
  };

  return (
    <div>
      <canvas
        width={WIDTH}
        height={HEIGHT}
        className="bg-gray-800"
        ref={canvasElementRef}
      ></canvas>
      <audio
        ref={audioElementRef}
        src={props.src}
        // controls
        autoPlay
        onPlay={onPlay}
        onEnded={onEnded}
        className=""
      ></audio>
    </div>
  );
};

export default AudioElement;
