import { FC } from "react";
import AudioElement from "../AudioElement/AudioElement";

const IuvFace: FC = () => {
  const state: string = "idle";

  const renderSwitch = (): JSX.Element => {
    switch (state) {
      case "playback":
        // return <AudioElement src=""></AudioElement>;
        return <div></div>;
      case "record":
        // return <AudioElement src=""></AudioElement>;
        return <div></div>;
      default:
        return (
          <div
            className="border-cyan-200 animate-pulse inline-block w-20 h-20 border-4 rounded-full blur-sm shadow-lg"
            role="status"
          ></div>
        );
    }
  };

  return (
    <div className="flex justify-center items-center">{renderSwitch()}</div>
  );
};

export default IuvFace;
