import { VFC, MouseEventHandler } from "react";
import { useState, useEffect } from "react";
interface RecordButtonProps {
  setRecordingWrapper: Function;
  disabled: boolean;
}

const RecordButton: VFC<RecordButtonProps> = (props) => {
  // store the enabled prop in local state
  const [disabled, setDisabled] = useState(props.disabled);
  // sync prop with local state
  useEffect(() => {
    if (props.disabled !== disabled) {
      setDisabled(props.disabled);
    }
  }, [props.disabled]);

  let notRecordingClass =
    "bg-gray-100 rounded-full p-8 focus:outline-none shadow-2xl disabled:opacity-10";
  // console.log(notRecordingClass);
  let recordingClass =
    "bg-red-500 rounded-full p-8 focus:outline-none shadow-2xl";
  const [className, setClassName] = useState(notRecordingClass);

  const onMouseDown = (event: React.MouseEvent) => {
    setClassName(recordingClass);
    props.setRecordingWrapper(true);
  };

  const onMouseUp = (event: React.MouseEvent) => {
    setClassName(notRecordingClass);
    props.setRecordingWrapper(false);
  };

  return (
    <button
      className={className}
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      disabled={props.disabled}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="h-6 w-6 text-gray-700"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M19 11a7 7 0 01-7 7m0 0a7 7 0 01-7-7m7 7v4m0 0H8m4 0h4m-4-8a3 3 0 01-3-3V5a3 3 0 116 0v6a3 3 0 01-3 3z"
        />
      </svg>
    </button>
  );
};

export default RecordButton;
