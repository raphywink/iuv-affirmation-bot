import React, { VFC } from "react";
import ChatWidgetWindowBody from "./ChatWidgetWindowBody";
import ChatWidgetWindowFooter from "./ChatWidgetWindowFooter";
import ChatWidgetWindowHeader from "./ChatWidgetWindowHeader";
import { ClientImpl } from "../client";
export interface ChatWidgetWindowProps {
  onClose: () => unknown;
  client: ClientImpl;
  disabled: boolean;
}

const ChatWidgetWindow: VFC<ChatWidgetWindowProps> = (props) => {
  return (
    <div className="sm:mb-6 flex-1 overflow-y-hidden flex flex-col border border-gray-400 rounded-xl sm:w-96 shadow-xl h-96">
      <ChatWidgetWindowHeader onClose={props.onClose} />
      <ChatWidgetWindowBody client={props.client} />
      <ChatWidgetWindowFooter client={props.client} disabled={props.disabled} />
    </div>
  );
};

export default ChatWidgetWindow;
