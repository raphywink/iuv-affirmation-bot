import { VFC } from "react";
import ChatWidgetConversation from "../conversation/ChatWidgetConversation";
import { ClientImpl } from "../client";

export interface ChatWidgetWindowBodyProps {
  client: ClientImpl;
}

const ChatWidgetWindowBody: VFC<ChatWidgetWindowBodyProps> = (props) => {
  return (
    <div className="flex-1 overflow-hidden flex bg-gray-300">
      <ChatWidgetConversation client={props.client} />
    </div>
  );
};

export default ChatWidgetWindowBody;
