// import { AudioRecorder, Client, SpeechRecognizer } from '@jovotech/client-web';
// import { action, computed, configure, makeObservable, observable } from 'mobx';

import { action, makeObservable, observable } from "mobx";
import {
  RequestConversationPart,
  ResponseConversationPart,
  ConversationPart,
} from "./types";

// There are problems with the current implementation...
// async methods break mobx and therefore the observable is changed from outside an action
// because the ticks after an await call are not in the action-context anymore.
// configure({
//   enforceActions: 'never',
// });

export class ClientImpl {
  private bpUrl: string;
  private bpAuthEmail: string;
  private bpAuthPwd: string;
  private bpBotName: string;
  private bpJwt: string | undefined;
  private requestCounter = 0;
  private responseCounter = 0;
  private initialized = false;

  // todo place in single array?
  public conversation: ConversationPart[] = [];
  public waitingForResponse = false;

  constructor() {
    if (
      process.env.NODE_ENV == "development" &&
      process.env.REACT_APP_BP_AUTH_EMAIL &&
      process.env.REACT_APP_BP_AUTH_PWD &&
      process.env.REACT_APP_BP_URL &&
      process.env.REACT_APP_BP_BOT_NAME
    ) {
      this.bpAuthEmail = process.env.REACT_APP_BP_AUTH_EMAIL;
      this.bpAuthPwd = process.env.REACT_APP_BP_AUTH_PWD;
      this.bpUrl = process.env.REACT_APP_BP_URL;
      this.bpBotName = process.env.REACT_APP_BP_BOT_NAME;
    } else {
      // TODO get via secret fetch or throw error
      this.bpAuthEmail = "";
      this.bpAuthPwd = "";
      this.bpUrl = "";
      this.bpBotName = "";
      console.error("not implemented yet");
    }

    makeObservable<ConversationPart[] & any>(this, {
      conversation: observable,
      addToConversation: action,
    });
  }

  public initialize = async () => {
    if (this.initialized) return;
    try {
      let resp = await fetch(this.bpUrl + "/api/v1/auth/login/basic/default", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: this.bpAuthEmail,
          password: this.bpAuthPwd,
        }),
      });
      let resp_json = await resp.json();
      this.bpJwt = resp_json.payload.jwt;
    } catch (err) {
      console.log(err);
      throw new Error("Bad response from server");
    }
    // await this.send("INIT DIALOG");
    this.initialized = true;
  };

  public addToConversation = (itemToAdd: ConversationPart) => {
    this.conversation.push(itemToAdd);
  };

  public send = async (text: string) => {
    this.requestCounter += 1;
    // ad to conversation
    const request: RequestConversationPart = {
      id: this.requestCounter,
      type: "request",
      data: {
        text: text,
        timestamp: Date(),
      },
    };
    this.addToConversation(request);
    try {
      this.waitingForResponse = true;
      let resp = await fetch(
        this.bpUrl +
          "/api/v1/bots/" +
          this.bpBotName +
          "/converse/" +
          this.bpAuthEmail +
          "", // "/secured?include=nlu,state,suggestions,decision",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + this.bpJwt,
          },
          body: JSON.stringify({
            text: text,
          }),
        }
      );
      let resp_parsed = await resp.json();
      this.responseCounter += 1;
      const response: ResponseConversationPart = {
        id: this.responseCounter,
        type: "response",
        data: {
          text: resp_parsed.responses[0].text,
          timestamp: Date(),
        },
      };
      this.addToConversation(response);
      this.waitingForResponse = false;
    } catch (err) {
      throw new Error("Bad response from server");
    }
  };

  public getRequestCounter = (): number => {
    return this.requestCounter;
  };

  public incrementRequestCounter = () => {
    this.requestCounter += 1;
  };
}

// export singleton
export const client = new ClientImpl();

// {
//   output: {
//     audioPlayer: {
//       enabled: false,
//     },
//     speechSynthesizer: {
//       enabled: false,
//     },
//     reprompts: {
//       enabled: false,
//     },
//   },
//   store: {
//     shouldPersistSession: false,
//   },
// }

// makeObservable<AudioRecorder & any>(client.audioRecorder, {
//   initialized: observable,
//   isInitialized: computed,
//   initialize: action,
//   recording: observable,
//   isRecording: computed,
//   start: action,
//   stop: action,
//   abort: action,
// });

// makeObservable<SpeechRecognizer & any>(client.speechRecognizer, {
//   recording: observable,
//   isRecording: computed,
//   start: action,
//   stop: action,
//   abort: action,
//   setupSpeechRecognition: action,
// });

// export default makeObservable<Client & any>(client, {
//   initialized: observable,
//   isInitialized: computed,
//   initialize: action,
//   audioRecorder: observable.deep,
//   speechRecognizer: observable.deep,
// }) as Client;
