// import { ClientEvent } from '@jovotech/client-web';
import { useContext, useEffect, useRef, VFC } from "react";
// import { client } from "../client";
// import { ConversationContext } from "../ChatWidget";
import ChatWidgetConversationPart from "./ChatWidgetConversationPart";
import { observer } from "mobx-react";
import { ClientImpl } from "../client";

export interface ChatWidgetConversationProps {
  client: ClientImpl;
}

const ChatWidgetConversation: VFC<ChatWidgetConversationProps> = observer(
  ({ client }) => {
    // const conversationParts = useContext(ConversationContext);

    // const conversationPartsWithQuickRepliesOnlyInLast = conversationParts.map(
    //   (part, index) => {
    //     return index !== conversationParts.length - 1 && part.type === "response"
    //       ? { ...part, data: { ...part.data, quickReplies: undefined } }
    //       : part;
    //   }
    // );

    const element = useRef<HTMLDivElement>(null);

    const scrollToBottom = () => {
      if (!element.current) return;
      element.current.scrollTop = element.current.scrollHeight;
    };

    useEffect(() => {
      scrollToBottom();
      // console.log(client.conversation[1]);
    }, [client.conversation.length]);

    return (
      <div
        ref={element}
        className="flex-grow flex flex-col space-y-4 px-6 py-4 overflow-y-scroll scrollbar-invisible hover:scrollbar"
      >
        {/* {conversationPartsWithQuickRepliesOnlyInLast.map((part, index) => (
        <ChatWidgetConversationPart part={part} key={index} />
      ))} */}
        {client.conversation.map((part, index) => (
          <ChatWidgetConversationPart part={part} key={index} />
        ))}
      </div>
    );
  }
);

export default ChatWidgetConversation;
