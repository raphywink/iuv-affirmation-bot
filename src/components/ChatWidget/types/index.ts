// import { Input, NormalizedOutputTemplate } from '@jovotech/client-web';

export interface RequestConversationPart {
  id: number;
  type: "request";
  data: any; // Input;
}

export interface ResponseConversationPart {
  id: number;
  type: "response";
  data: any; // NormalizedOutputTemplate;
}

export type ConversationPart =
  | RequestConversationPart
  | ResponseConversationPart;
