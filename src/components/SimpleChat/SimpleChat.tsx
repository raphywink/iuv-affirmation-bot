import "./SimpleChat.css";
import { FC } from "react";
import React from "react";
import { useState } from "react";
import logo from "./message-logo.svg"; // gives image path
// import internal from 'stream';

interface processedMessage {
  key: number;
  message: string;
  label: string;
  score: string;
}

const SimpleChat: FC = () => {
  const [list, setList] = useState<processedMessage[]>([]);
  const [value, setValue] = useState<string>("");
  const [messageCounter, setMessageCounter] = useState<number>(1);

  const addToList = () => {
    let tempArr = list;

    tempArr.push({
      key: messageCounter,
      message: value,
      label: "",
      score: "",
    });
    setMessageCounter(messageCounter + 1);

    fetch(
      "https://m7243acqnj.execute-api.us-east-1.amazonaws.com/prod/distilbert-base-uncased-finetuned-sst-2-english",
      {
        // Adding method type
        method: "POST",
        // Adding body or contents to send
        body: JSON.stringify({
          inputs: value,
        }),
        // Adding headers to the request
        headers: {
          "Content-type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => console.log(json));

    setList(tempArr);

    setValue("");
  };

  // const deleteItem = (index) => {

  //   let temp = list.filter((item, i) => i !== index);

  //   setList(temp);

  // };

  return (
    <div>
      <ul>
        {list.map((item) => {
          return (
            <React.Fragment key={item.key}>
              <div className="chat-notification">
                <div className="chat-notification-logo-wrapper">
                  <img
                    className="chat-notification-logo"
                    src={logo}
                    alt="message logo"
                  />
                </div>
                <div className="chat-notification-content">
                  <h4 className="chat-notification-title">
                    Your message: <i>{item.message}</i>
                  </h4>
                  <p className="chat-notification-message">
                    Sentiment classif. result
                    <br />- label: <i>{item.label}</i>
                    <br />- score: <i>{item.score}</i>
                  </p>
                </div>
              </div>
              <br />
            </React.Fragment>
          );
        })}
      </ul>
      <input
        className="px-4 py-1 text-sm text-purple-600 font-semibold rounded-full border border-purple-200 hover:text-white hover:bg-purple-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2"
        type="text"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onKeyDown={(e) => e.key === "Enter" && addToList()}
      />{" "}
      <br />
      <button
        className="px-4 py-1 text-sm text-purple-600 font-semibold rounded-full border border-purple-200 hover:text-white hover:bg-purple-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2"
        onClick={addToList}
      >
        send
      </button>
    </div>
  );
};

export default SimpleChat;
