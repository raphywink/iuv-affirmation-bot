import { FC } from "react";

import companyLogo from "./assets/VUIlogo.png";

const VUIlogo: FC = () => {
  return (
    // todo: stop from shrinking
    <div className="grid grid-cols-3 gap-1">
      <div></div>
      <div className="bg-slate-300 rounded-xl m-1">
        <img
          className="w-60 my-4"
          src={companyLogo}
          alt="VUI.agency logo"
        ></img>
      </div>
      <div></div>
    </div>
  );
};

export default VUIlogo;
