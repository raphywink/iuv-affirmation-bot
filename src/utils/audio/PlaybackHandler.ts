export class PlayBackHandler {
  private isPlaying: boolean;
  private audioContext?: AudioContext;

  constructor() {
    this.isPlaying = false;

    try {
      let AudioContext = window.AudioContext;
      this.audioContext = new AudioContext();
    } catch (e) {
      alert(
        "Error loading the AudioContext (could mean your browser does not support the HTML5 webaudio API):" +
          e
      );
    }
    // this.playAudioFromURL("http://bla.com");
  }

  // private playAudioFromURL = (url: string) => {
  //   console.log(url);
  //   // this.curSource = this.audioContext.createBufferSource();
  //   // this.curSource.buffer = this.audioBuffer;
  //   // this.curSource.connect(this.audioContext.destination);
  //   // this.curSource.start(0, startTime, durTime);
  //   // this.curSource.onended = () => {
  //   //     this.isPlaying = false;
  //   // };
  // };
}
