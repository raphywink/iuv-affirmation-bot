// import { Stream, StreamOptions } from "stream";
import { action, makeObservable, observable } from "mobx";

import { Console } from "console";
import { stringify } from "querystring";
import { Url } from "url";
import { client } from "../../components/ChatWidget/client";

export class InputStreamProcessorImpl {
  public asrCurResult = "";

  public asrFinalResult = "";

  private recording: boolean;

  private appendDanglingBuffer: boolean;

  private counter: number;

  private apiUrl: string;

  private connectedToApi: boolean;

  private deepgramApiKey: string;

  private deepgramProjectID: string;

  private waitingForFinalResponse = false;

  constructor() {
    this.recording = false;
    this.appendDanglingBuffer = false;

    this.counter = 0;
    this.apiUrl = "wss://api.deepgram.com/v1/listen?model=meeting";
    if (
      process.env.REACT_APP_DEPPGRAM_API_KEY &&
      process.env.REACT_APP_DEPPGRAM_PROJECT_ID
    ) {
      this.deepgramProjectID = process.env.REACT_APP_DEPPGRAM_PROJECT_ID;
      this.deepgramApiKey = process.env.REACT_APP_DEPPGRAM_API_KEY;
    } else {
      // TODO set via SECRET fetch
      this.deepgramProjectID = "";
      this.deepgramApiKey = "";
      alert("this.deepgramProjectID not set");
    }
    this.connectedToApi = false;

    makeObservable<string & any>(this, {
      asrCurResult: observable,
      setAsrCurResult: action,
    });

    // makeObservable<string & any>(this, {
    //   asrFinalResult: observable,
    //   setAsrFinalResult: action,
    // });
  }

  ///////////////////////////////////////////
  // public api

  public setAsrCurResult = (text: string) => {
    this.asrCurResult = text;
  };

  // public setAsrFinalResult = (text: string) => {
  //   console.log("setAsrFinalResult: " + text);
  //   this.asrCurResult = text;
  // };

  public setRecording = (state: boolean) => {
    this.recording = state;
    console.log(state);
    if (!state) {
      this.appendDanglingBuffer = true;
      // this.setAsrFinalResult(this.asrCurResult);
      // this.setAsrCurResult("");
    } else {
      this.startRecording();
    }
  };

  public startRecording = () => {
    this.setAsrCurResult("");
    navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
      if (!MediaRecorder.isTypeSupported("audio/webm"))
        return alert("Browser not supported");
      const mediaRecorder = new MediaRecorder(stream, {
        mimeType: "audio/webm",
      });
      const socket = new WebSocket(
        "wss://api.deepgram.com/v1/listen?model=conversationalai&interim_results=true", //
        ["token", this.deepgramApiKey]
      );
      socket.onopen = () => {
        // document.querySelector("#status").textContent = "Connected";
        console.log({ event: "onopen" });
        mediaRecorder.addEventListener("dataavailable", async (event) => {
          if (
            (this.recording && event.data.size > 0 && socket.readyState == 1) ||
            (event.data.size > 0 &&
              socket.readyState == 1 &&
              this.appendDanglingBuffer)
          ) {
            console.log("sending packet");
            socket.send(event.data);
            if (this.appendDanglingBuffer) {
              console.log("appended dangling buffer -> closing");
              this.appendDanglingBuffer = false;
              // close connection after
              setTimeout(() => {
                console.log("closing ws");
                if (this.asrCurResult !== "") client.send(this.asrCurResult);
                this.setAsrCurResult("");
                mediaRecorder.stop();
                socket.close();
              }, 2000);

              // this.waitingForFinalResponse = true;
            }
          }
        });
        mediaRecorder.start(250);
      };

      socket.onmessage = (message) => {
        const received = JSON.parse(message.data);
        const transcript = received.channel.alternatives[0].transcript;
        if (transcript && received.is_final) {
          console.log("here is the transcript");
          console.log(received);
          this.setAsrCurResult(this.asrCurResult + " " + transcript);
          if (!this.recording) {
          }
          //   document.querySelector("#transcript").textContent +=
          // transcript + " ";
          // if (this.waitingForFinalResponse) {
          //   socket.close();
          //   this.waitingForFinalResponse = this.waitingForFinalResponse;
          // }
        }
      };

      socket.onclose = () => {
        console.log({ event: "onclose" });
      };

      socket.onerror = (error) => {
        console.log({ event: "onerror", error });
      };
      //   this.mediaRecorder.addEventListener("dataavailable", async (event) => {
      //     this.counter = this.counter + 1;
      //     if (
      //       (event.data.size > 0 &&
      //         this.recording &&
      //         this.ws?.readyState === 1) ||
      //       (event.data.size > 0 && this.appendDanglingBuffer)
      //     ) {
      //       // sent to api code goes here
      //       console.log(event.data);
      //       console.log(this.ws);
      //       this.ws!.send("wtf");
      //       if (this.appendDanglingBuffer) {
      //         this.appendDanglingBuffer = false;
      //         // close connection
      //         this.ws?.close();
      //       }
      //     }
      //   });

      //   this.mediaRecorder.start(250);
    });
  };
}

export const inputStreamProcessor = new InputStreamProcessorImpl();
