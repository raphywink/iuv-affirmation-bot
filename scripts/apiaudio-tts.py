#pip install -U apiaudio
import apiaudio
import shutil
import subprocess
import json
from dotenv import dotenv_values
import os.path
import hashlib

def create_audio_text_file_pairs(
    text,
    audio_dir,
    dotenv_vals,
    greeting_text = False):
    if not greeting_text:
        text_md5_sum = hashlib.md5(text.encode('utf-8')).hexdigest() 
    else:
        text_md5_sum = "welcome_message"
    audio_file_name = text_md5_sum + ".mp3"
    txt_file_name = text_md5_sum + ".txt"
    # print(audio_file_name)

    if not os.path.exists(os.path.join(audio_dir, audio_file_name)):
        print("generating audio file for: \n\t" + text)

        apiaudio.api_key = dotenv_vals["APIAUDIO_API_KEY"]

        script = apiaudio.Script.create(
            scriptText = text
            )
        
        # use this in case soundTemplate is used
        # """
        # <<soundSegment::intro>><<sectionName::intro>>
        # <<soundSegment::main>><<sectionName::main>>
        # %s
        # <<soundSegment::outro>><<sectionName::outro>>
        #     """ %text)
        apiaudio.Speech.create(
            scriptId = script.get("scriptId"), 
            voice = "aria" 
        )
        if greeting_text:
            soundTemplate = "openup"
        else:
            soundTemplate = ""
        
        audio = apiaudio.Mastering.create(
            scriptId=script.get("scriptId"),
            soundTemplate = soundTemplate
        )
        # Download the file from `url` and save it locally under `audio_file_name`:
        # using wget + system call because `url` is a redirect and 
        # can't get requests lib to work
        wget_command = 'wget '  + audio['url'] + ' -O ' + os.path.join(audio_dir, audio_file_name)
        res = subprocess.call(wget_command, shell = True)

        # remove silence from start and end using sox
        # see https://digitalcardboard.com/blog/2009/08/25/the-sox-of-silence/comment-page-2/
        trim_start_fp = os.path.join(audio_dir, text_md5_sum + '_trim.mp3')
        sox_command = 'sox ' + os.path.join(audio_dir, audio_file_name) + ' ' + trim_start_fp + ' silence 1 0.1 1% reverse silence 1 0.1 1% reverse'
        res = subprocess.call(sox_command, shell = True)

        # save txt file along side it contain the said utterance
        with open(os.path.join(audio_dir, txt_file_name), 'w', encoding='utf-8') as f:
            f.write(text)

if __name__ == "__main__":
    
    audio_dir = "audio"
    
    # read dotenv file
    dotenv_vals = dotenv_values("../.envrc")
    text = """
        <<soundSegment::intro>><<sectionName::intro>>
        Hi there!
        I am the Intelligent User Voice assistant or IUV for short. 
        <<soundSegment::main>><<sectionName::main>>
        I was made by the voice user interface agency
        <<soundSegment::outro>><<sectionName::outro>>
        I currently simply affirm everything you say
    """
    # create welcome text
    create_audio_text_file_pairs(
        text = text,
        audio_dir = audio_dir,
        dotenv_vals = dotenv_vals,
        greeting_text = True
    )

    # read builtin_text.json to extract all builtin_text
    path2json = os.path.join(
        dotenv_vals["BP_LOCAL_INSTALL_LOCATION"],
        "data",
        "bots",
        dotenv_vals["REACT_APP_BP_BOT_NAME"],
        "content-elements",
        "builtin_text.json")
    print(path2json)
    with open(path2json, 'r') as f:
        data = f.read()

    builtin_text = json.loads(data)
    # loop through entries
    for bit in builtin_text:
        text = bit["formData"]["text$en"]
        greeting_text = False
        
        create_audio_text_file_pairs(
            text = text,
            audio_dir = audio_dir,
            dotenv_vals = dotenv_vals,
            greeting_text = greeting_text
        )
        # and create variations
        for variation in bit["formData"]['variations$en']:
            create_audio_text_file_pairs(
                text = variation,
                audio_dir = audio_dir,
                dotenv_vals = dotenv_vals,
                greeting_text = greeting_text
            )

        


    # extract builtin texts
